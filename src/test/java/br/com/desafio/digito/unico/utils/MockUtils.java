package br.com.desafio.digito.unico.utils;

import br.com.desafio.digito.unico.model.Criptografia;
import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.CriptografiaDTO;
import br.com.desafio.digito.unico.model.dto.DigitoUnicoDTO;

import java.util.Collections;

public class MockUtils {

    public static Usuario montaUsuario() {
        return Usuario
                .builder()
                .nome("Jose Antonio da Silva")
                .email("joseantonio@teste.com")
                .digitoUnicos(Collections.singletonList(montaDigitoUnico()))
                .build();

    }

    public static DigitoUnico montaDigitoUnico(){
        return DigitoUnico
                .builder()
                .entradaCalculo("9875")
                .resultado(2)
                .build();
    }

    public static DigitoUnico montaDigitoUnicoComParametros(String entradaCalculo, Integer resultado){
        return DigitoUnico
                .builder()
                .entradaCalculo(entradaCalculo)
                .resultado(resultado)
                .build();
    }

    public static DigitoUnicoDTO montaDigitoUnicoDTOComParametros(String entradaCalculo, Integer numeroVezes, Integer IdUsuario){
        return DigitoUnicoDTO
                .builder()
                .entradaCalculo(entradaCalculo)
                .numeroVezes(numeroVezes)
                .idUsuario(IdUsuario)
                .build();
    }

    public static Criptografia montaCriptografia(){
        return Criptografia
                .builder()
                .chavePublica("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg56+x+MYG/Rb15J55rUXtag3stEp1mrZ1244hkHtkPKqBdYe7xas2C6gWWi4Pp7RFJRi7twImwFFIT4RVP" +
                        "m+dKpC4zaAT1KEWz9ltL/zS+yyd6c+nTH8gGiAiTWTn2wTboGUhYqwNaenVH1pBtB3H3yN8MdGtnyFjYNTq+LslL9s6SUuCYsuVVytRtsvxe1A9u9+3zhdb0kb+WMmDtOM2t" +
                        "nh5WGnge1WPs70lq5OFXoZdfqTWm0wTGUW5nUiEKLUUL2Db1ZuRzWqAN1jtHXONrJld0FGf+0PySRMkaeIkNyr7gOi6KXn/3BC1rVu+ZFoNfOMgpXhUqyK7HmAxp4OZQIDAQAB")
                .chavePrivada("MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCDnr7H4xgb9FvXknnmtRe1qDey0SnWatnXbjiGQe2Q8qoF1h7vFqzYLqBZaLg+ntEUlGLu" +
                        "3AibAUUhPhFU+b50qkLjNoBPUoRbP2W0v/NL7LJ3pz6dMfyAaICJNZOfbBNugZSFirA1p6dUfWkG0HcffI3wx0a2fIWNg1Or4uyUv2zpJS4Jiy5VXK1G2y/F7UD2737fOF1vS" +
                        "Rv5YyYO04za2eHlYaeB7VY+zvSWrk4Vehl1+pNabTBMZRbmdSIQotRQvYNvVm5HNaoA3WO0dc42smV3QUZ/7Q/JJEyRp4iQ3KvuA6Lopef/cELWtW75kWg184yCleFSrIrseY" +
                        "DGng5lAgMBAAECggEAcgwUfblOQ7K5/hIi5ICYOcS39o789JkDh4MvIU5xHn7WSlZd2YfJX00PjkmKFGCjBHULQLOSFVXFY+eXNw1sNUTuzliEV7dFjpdhdnw4wdooTjuxqp4M" +
                        "j+t9LOcqsiIAuGk5m3kv9F4ARNqiLdBm1/aiOzdBE5DUXGmmD7OCkeQHVMH6i8dU2NMJqkxOzJDn89vqRQLLF9wJE3mVpKaRQj+owq+0a7g0RBc5u9nr1VUCwJHeLEUcPO0dsyx" +
                        "gbtfXhJECYxFw3YnZNwQaV94FCFlXbn57MFLxxH2pFf+gG1ROaCZkTlilG/HKHjvODOKPRCbC5XL3M7MAl/FoWNvHxQKBgQDUuBttlkIqFYppg+a70kR0n8AVC3L5nQsfxRhfhq" +
                        "0xeQp7wNoqvQZTff2H2faDgChRDPYS+2w+H+y42o7uAcRr/N/D/6qMgOLAzoJxB5DUPC/Drx+nnE3jo9qbYESnWRso8hyZtcckRh/UeJQamzaCoGPhdSkSUgpla1qPG3O5UwKBg" +
                        "QCeZm9ROmzpXMlHJB4WzVHMjtn8HCzJYXaLM9xgcy+z3gxsvIfHqO1N2OzOvgt8M0PaPv2S3fs5E5fRP78J+o3dWdGpb7w8zmvyA93Xv6/c4JUpQAhwox6g+ZWsU2MvkqVVN0nK0" +
                        "TGnBv9uBT2Gust9YsqIzbxPYDsN9qV32IHKZwKBgAVseE6QATnfeLr+APkTAGnU1On0c8cHiQwTU27EGVBknK7RGP7z+OBSGw/Gr/FkaCLA6NlNDdJ7r+Z6tCA/bUmvlekCcM/KT" +
                        "X3LBippnfi7XCBqK61zX80JFX7WsoltzTJ7Ed2dc+lp3E/njycqg/ZKrqFGAnkUHG2/kfyKH6Y/AoGBAI8+7Z5UqtejTHbkh71xMm8+JI3k4O/BDxY18S7cHCCaRzwyhswdzXTgHs" +
                        "+ceAB6z/rXijO+QYnNWTcnYdlhV9f8eSm0XVPUDTSRIJcSx7SE+P+T484RBgrnit6LjqAgCxp0ZGxhIHj/UBGr9YcY+BtNMwHipFBYIy+uhNdL2ZwzAoGAZczUfwSXUFdf5/yDynl" +
                        "1psB5xxUqK7I+5WuHfczIo4eDwkKdZpOJA9dsPMV/ksSv/Ykgd8GfaG2OWQttwZMCDiNzHJ5u3/i67rMi3VuS5Aim6QeMwTFICxEh0STLveh/UzgyagCvUKGlMOT75Y/7g+Rg8rvq" +
                        "Cav7b9XiQCswVQE=")
                .dadosCriptografados(new byte[4])
                .build();

    }

    public static CriptografiaDTO montaCriptografiaDTO(){
        return CriptografiaDTO
                .builder()
                .chavePublica("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg56+x+MYG/Rb15J55rUXtag3stEp1mrZ1244hkHtkPKqBdYe7xas2C6gWWi4Pp7RFJRi7twImwFFIT4RVP" +
                        "m+dKpC4zaAT1KEWz9ltL/zS+yyd6c+nTH8gGiAiTWTn2wTboGUhYqwNaenVH1pBtB3H3yN8MdGtnyFjYNTq+LslL9s6SUuCYsuVVytRtsvxe1A9u9+3zhdb0kb+WMmDtOM2t" +
                        "nh5WGnge1WPs70lq5OFXoZdfqTWm0wTGUW5nUiEKLUUL2Db1ZuRzWqAN1jtHXONrJld0FGf+0PySRMkaeIkNyr7gOi6KXn/3BC1rVu+ZFoNfOMgpXhUqyK7HmAxp4OZQIDAQAB")
                .dadosDescriptografados("Nome: Jose Antonio da Silva\nEmail: joseantonio@teste.com")
                .build();
    }

}

package br.com.desafio.digito.unico.resource;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.dto.DigitoUnicoDTO;
import br.com.desafio.digito.unico.service.implementation.DigitoUnicoServiceImp;
import br.com.desafio.digito.unico.utils.MockUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(DigitoUnicoResource.class)
public class DigitoUnicoResourceTeste {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DigitoUnicoServiceImp digitoUnicoServiceImp;

    ObjectMapper objectMapper;

    private DigitoUnico digitoUnico;

    private DigitoUnicoDTO digitoUnicoDTO;

    @Before
    public void init(){
      objectMapper = new ObjectMapper();
      digitoUnicoDTO = MockUtils.montaDigitoUnicoDTOComParametros("9875", 4, 1);
      digitoUnico = MockUtils.montaDigitoUnicoComParametros("9875987598759875", 8);
      digitoUnico.setId(1);
    }

    @Test
    public void validarCalculoDigitoUnicoTeste() throws Exception {
        Mockito.when(this.digitoUnicoServiceImp.validarCalculoDigitoUnico(digitoUnicoDTO)).thenReturn(digitoUnico);
        this.mvc.perform(MockMvcRequestBuilders.post("/digitounicos/calcular")
                .content(objectMapper.writeValueAsString(digitoUnicoDTO))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['entradaCalculo']", Matchers.equalToIgnoringWhiteSpace(digitoUnico.getEntradaCalculo())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['resultado']", Matchers.is(digitoUnico.getResultado())));
    }

    @Test
    public void validarCalculoDigitoUnicoSemSucessoTeste() throws Exception {
        digitoUnicoDTO.setEntradaCalculo("$$$");
        Mockito.when(this.digitoUnicoServiceImp.validarCalculoDigitoUnico(digitoUnicoDTO)).thenReturn(digitoUnico);
        this.mvc.perform(MockMvcRequestBuilders.post("/digitounicos/calcular")
                .content(objectMapper.writeValueAsString(digitoUnicoDTO))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}

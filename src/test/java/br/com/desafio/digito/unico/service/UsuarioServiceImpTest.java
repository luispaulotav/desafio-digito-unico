package br.com.desafio.digito.unico.service;

import br.com.desafio.digito.unico.exception.NotFoundException;
import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.repository.UsuarioRepository;
import br.com.desafio.digito.unico.service.implementation.UsuarioServiceImp;
import br.com.desafio.digito.unico.utils.MockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UsuarioServiceImpTest {

    @InjectMocks
    private UsuarioServiceImp usuarioService;

    @Mock
    private UsuarioRepository usuarioRepository;

    private Usuario usuario;

    private List<Usuario> usuarios = new ArrayList<>();

    private DigitoUnico digitoUnico;

    private DigitoUnico digitoUnico2;

    private List<DigitoUnico> digitoUnicos = new ArrayList<>();

    @Before
    public void init(){
       usuario = MockUtils.montaUsuario();
       usuario.setId(1);
       usuarios.add(usuario);

        digitoUnico = MockUtils.montaDigitoUnicoComParametros("9875", 2);
        digitoUnicos.add(digitoUnico);
        digitoUnico2 = MockUtils.montaDigitoUnicoComParametros("9875987598759875", 8);
        digitoUnicos.add(digitoUnico2);
        usuario.setDigitoUnicos(digitoUnicos);
    }

    @Test
    public void salvarUsuarioTeste() {
      usuarioService.salvar(usuario);
      assertEquals(usuario.getNome(), "Jose Antonio da Silva");
      assertEquals(usuario.getEmail(), "joseantonio@teste.com");
      assertNotNull(usuario.getId());
    }

    @Test
    public void editarUsuarioTeste() {
        when(usuarioRepository.findById(usuario.getId())).thenReturn(Optional.of(usuario));
        assertEquals(usuario.getEmail(), "joseantonio@teste.com");
        assertEquals(Optional.ofNullable(usuario.getId()), Optional.ofNullable(1));
        assertNotNull(usuario.getId());

        usuario.setEmail("joseantonio123@gmail.com");

        usuarioService.editar(usuario.getId(), usuario);
        assertEquals("joseantonio123@gmail.com", usuario.getEmail());
        assertEquals("Jose Antonio da Silva", usuario.getNome());
        assertNotNull(usuario.getDigitoUnicos());
        assertEquals(Optional.ofNullable(usuario.getId()), Optional.ofNullable(1));
    }

    @Test
    public void excluirUsuarioTeste(){
       when(usuarioService.buscarUsuario(1)).thenReturn(Optional.ofNullable(usuario));
       Boolean usuarioExcluido = usuarioService.excluir(usuario.getId());
       assertNotNull(usuarioExcluido);
       assertTrue(usuarioExcluido);
    }

    @Test(expected = NotFoundException.class)
    public void excluirUsuarioInexistenteTeste(){
        usuarioService.excluir(2);
    }

    @Test
    public void buscaUsuarioPorIdTeste(){
        when(usuarioService.buscarUsuario(1)).thenReturn(Optional.ofNullable(usuario));
        usuarioService.buscarUsuario(1);

        assertEquals("joseantonio@teste.com", usuario.getEmail());
        assertEquals("Jose Antonio da Silva", usuario.getNome());
        assertNotNull(usuario.getDigitoUnicos());
    }

    @Test
    public void buscaTodosUsuariosTeste(){
        Page<Usuario> usuariosPage = new PageImpl<>(usuarios);
        when(usuarioRepository.findAll(usuariosPage.getPageable()))
                .thenReturn(usuariosPage);
        usuarioService.buscarTodos(usuariosPage.getPageable());

        assertNotNull(usuariosPage);
        assertEquals(usuariosPage.getTotalElements(), usuarios.size());
        assertEquals(usuariosPage.getContent().get(0).getId(), usuarios.get(0).getId());
    }

    @Test
    public void buscarDigitoUnicoPorIdUsuarioTeste() {
        when(usuarioService.buscarUsuario(1)).thenReturn(Optional.ofNullable(usuario));
        usuarioService.buscarDigitoUnicoPorIdUsuario(1);

        assertEquals("joseantonio@teste.com", usuario.getEmail());
        assertEquals("Jose Antonio da Silva", usuario.getNome());
        assertNotNull(usuario.getDigitoUnicos());
        assertEquals(digitoUnicos.size(), usuario.getDigitoUnicos().size());
        assertEquals(digitoUnicos.get(0).getResultado(), usuario.getDigitoUnicos().get(0).getResultado());
        assertEquals(digitoUnicos.get(0).getEntradaCalculo(), usuario.getDigitoUnicos().get(0).getEntradaCalculo());
    }

    @Test(expected = Exception.class)
    public void buscarDigitoUnicoPorIdNuloTeste() {
        usuario.setId(null);
        when(usuarioService.buscarDigitoUnicoPorIdUsuario(usuario.getId())).thenReturn(digitoUnicos);
        usuarioService.buscarDigitoUnicoPorIdUsuario(usuario.getId());
    }
}

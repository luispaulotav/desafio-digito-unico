package br.com.desafio.digito.unico.service;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.DigitoUnicoDTO;
import br.com.desafio.digito.unico.repository.DigitoUnicoRepository;
import br.com.desafio.digito.unico.repository.UsuarioRepository;
import br.com.desafio.digito.unico.service.implementation.DigitoUnicoServiceImp;
import br.com.desafio.digito.unico.utils.MockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class DigitoUnicoServiceImpTeste {

    @InjectMocks
    private DigitoUnicoServiceImp digitoUnicoServiceImp;

    @Mock
    private DigitoUnicoRepository digitoUnicoRepository;

    @Mock
    private UsuarioRepository usuarioRepository;

    private DigitoUnico digitoUnico;

    private DigitoUnicoDTO digitoUnicoDTO;

    private Usuario usuario;

    @Before
    public void init(){
        usuario = MockUtils.montaUsuario();
        usuario.getDigitoUnicos().get(0).setId(1);
        usuario.setId(2);
        digitoUnicoDTO = MockUtils.montaDigitoUnicoDTOComParametros("9875", 4, 1);
        digitoUnico = MockUtils.montaDigitoUnicoComParametros("9875987598759875", 8);
        digitoUnico.setId(2);
    }

    @Test
    public void validarCalculoDigitoUnicoTeste(){
        Mockito.when(digitoUnicoRepository.save(digitoUnico)).thenReturn(digitoUnico);
        Mockito.when(usuarioRepository.findById(usuario.getId())).thenReturn(Optional.ofNullable(usuario));
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
        digitoUnicoServiceImp.validarCalculoDigitoUnico(digitoUnicoDTO);

        assertEquals(digitoUnico.getEntradaCalculo(), String.valueOf("9875987598759875"));
        assertEquals(digitoUnico.getResultado(), Integer.valueOf(8));
        assertNotNull(digitoUnico.getId());
    }

    @Test
    public void validarCalculoDigitoUnicoSemUsuarioTeste(){
        digitoUnicoDTO.setIdUsuario(null);
        Mockito.when(digitoUnicoRepository.save(digitoUnico)).thenReturn(digitoUnico);
        digitoUnicoServiceImp.validarCalculoDigitoUnico(digitoUnicoDTO);

        assertEquals(digitoUnico.getEntradaCalculo(), String.valueOf("9875987598759875"));
        assertEquals(digitoUnico.getResultado(), Integer.valueOf(8));
        assertNotNull(digitoUnico.getId());
    }

    @Test
    public void validarCalculoDigitoUnicoSemUsuarioSemNumeroRepeticaoTeste(){
        digitoUnicoDTO.setIdUsuario(null);
        digitoUnicoDTO.setNumeroVezes(null);
        Mockito.when(digitoUnicoRepository.save(digitoUnico)).thenReturn(digitoUnico);
        digitoUnicoServiceImp.validarCalculoDigitoUnico(digitoUnicoDTO);

        assertEquals(digitoUnico.getEntradaCalculo(), String.valueOf("9875987598759875"));
        assertEquals(digitoUnico.getResultado(), Integer.valueOf(8));
        assertNotNull(digitoUnico.getId());
    }
}

package br.com.desafio.digito.unico.repository;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.utils.MockUtils;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioRepositoryTeste {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    private Usuario usuario;

    private DigitoUnico digitoUnico;

    private List<Usuario> usuarios = new ArrayList<>();

    @Before
    public void init() {
        usuario = MockUtils.montaUsuario();
        digitoUnico = usuario.getDigitoUnicos().get(0);
        digitoUnicoRepository.save(digitoUnico);

        usuario.setDigitoUnicos(Lists.newArrayList(digitoUnico));
        usuarios.add(usuario);
    }

    @Test
    public void salvarUsuarioTeste(){
        usuarioRepository.save(usuario);
        assertEquals(usuario.getNome(), "Jose Antonio da Silva");
        assertEquals(usuario.getEmail(), "joseantonio@teste.com");
        assertNotNull(usuario.getId());
    }

    @Test
    public void editarUsuarioTeste(){
        usuario.setEmail("joseantonio123@teste.com");
        usuarioRepository.save(usuario);
        assertEquals(usuario.getNome(), "Jose Antonio da Silva");
        assertEquals(usuario.getEmail(), "joseantonio123@teste.com");
        assertNotNull(usuario.getId());
    }

    @Test
    public void excluirUsuarioTeste(){
        usuarioRepository.save(usuario);
        usuarioRepository.deleteById(usuario.getId());
        Assertions.assertThat(usuarioRepository.findById(usuario.getId())).isEmpty();
    }

    @Test
    public void buscarUsuarioPorIdTeste(){
        usuarioRepository.save(usuario);
        usuarioRepository.findById(usuario.getId());

        assertEquals(usuario.getNome(), "Jose Antonio da Silva");
        assertEquals(usuario.getEmail(), "joseantonio@teste.com");
        assertNotNull(usuario.getId());
    }

    @Test
    public void buscarTodosUsuariosTeste(){
        List<Usuario> usuariosFindAll = new ArrayList<>();
        usuarioRepository.saveAll(usuarios);
        usuariosFindAll.addAll(Lists.newArrayList(usuarioRepository.findAll()));

        Assertions.assertThat(usuarios).isNotNull().isNotEmpty();
        Assertions.assertThat(usuarios.size()).isEqualTo(usuarios.size());
        Assertions.assertThat(usuarios.get(0).getId()).isEqualTo(usuarios.get(0).getId());
    }
}

package br.com.desafio.digito.unico.resource;

import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.service.implementation.UsuarioServiceImp;
import br.com.desafio.digito.unico.utils.MockUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.isA;


@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioResource.class)
public class UsuarioResourceTeste {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UsuarioServiceImp usuarioServiceImp;

    ObjectMapper objectMapper;

    private Usuario usuario;

    private List<Usuario> usuarios = new ArrayList<>();

    @Before
    public void init(){
        objectMapper = new ObjectMapper();
        usuario = MockUtils.montaUsuario();
        usuario.setId(1);
        usuarios.add(usuario);
    }

    @Test
    public void salvarUsuarioTeste() throws Exception {
        Mockito.when(this.usuarioServiceImp.salvar(usuario)).thenReturn(usuario);
        this.mvc.perform(MockMvcRequestBuilders.post("/usuarios/salvar")
                .content(objectMapper.writeValueAsString(usuario))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(usuario.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['email']", Matchers.is(usuario.getEmail())));
    }

    @Test
    public void editarUsuarioTeste() throws Exception {
        Mockito.when(this.usuarioServiceImp.editar(usuario.getId(), usuario)).thenReturn(usuario);
        this.mvc.perform(MockMvcRequestBuilders.put("/usuarios/editar/" + usuario.getId())
                .content(objectMapper.writeValueAsString(usuario))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(usuario.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['email']", Matchers.is(usuario.getEmail())));
    }

    @Test
    public void excluirUsuarioTeste() throws Exception {
        Mockito.when(this.usuarioServiceImp.excluir(usuario.getId())).thenReturn(Boolean.TRUE);
        this.mvc.perform(MockMvcRequestBuilders.delete("/usuarios/excluir/" + usuario.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    public void buscarUsuarioPorIdTeste() throws Exception {
        Mockito.when(this.usuarioServiceImp.buscarUsuario(usuario.getId())).thenReturn(Optional.ofNullable(usuario));
        this.mvc.perform(MockMvcRequestBuilders.get("/usuarios/buscar/" + usuario.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$['id']", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$['nome']", Matchers.equalToIgnoringWhiteSpace(usuario.getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['email']", Matchers.is(usuario.getEmail())));
    }

    @Test
    public void buscarTodosUsuariosTeste() throws Exception {
        Mockito.when(this.usuarioServiceImp.buscarTodos(isA(Pageable.class))).thenReturn(new PageImpl<>(usuarios));
        this.mvc.perform(MockMvcRequestBuilders.get("/usuarios/buscartodos")
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['id']", Matchers.is(usuarios.get(0).getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['content'][0].['nome']", Matchers.is(usuarios.get(0).getNome())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['totalElements']",  Matchers.is(usuarios.size())));

    }
}

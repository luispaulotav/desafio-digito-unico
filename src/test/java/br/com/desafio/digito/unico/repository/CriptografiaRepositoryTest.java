package br.com.desafio.digito.unico.repository;

import br.com.desafio.digito.unico.model.Criptografia;
import br.com.desafio.digito.unico.utils.MockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CriptografiaRepositoryTest {

    @Autowired
    private CriptografiaRepository criptografiaRepository;

    private Criptografia criptografia;

    @Before
    public void init() {
        criptografia = MockUtils.montaCriptografia();
    }

    @Test
    public void salvarCriptografiaTeste() {
       criptografiaRepository.save(criptografia);

        assertNotNull(criptografia.getId());
        assertNotNull(criptografia.getDadosCriptografados());
        assertNotNull(criptografia.getChavePrivada());
        assertNotNull(criptografia.getChavePublica());
    }

    @Test
    public void buscaCriptografiaPorChavePublicaTeste(){
        criptografiaRepository.save(criptografia);
        criptografiaRepository.findCriptografiaByChavePublica(criptografia.getChavePublica());

        assertNotNull(criptografia.getId());
        assertNotNull(criptografia.getDadosCriptografados());
        assertNotNull(criptografia.getChavePrivada());
        assertNotNull(criptografia.getChavePublica());
    }

}

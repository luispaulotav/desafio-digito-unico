package br.com.desafio.digito.unico.repository;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.utils.MockUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DigitoUnicoRepositoryTeste {

    @Autowired
    private DigitoUnicoRepository digitoUnicoRepository;

    private DigitoUnico digitoUnico;

    @Before
    public void init() {
        digitoUnico = MockUtils.montaDigitoUnico();
    }

    @Test
    public void salvarDigitoUnicoTeste(){
       digitoUnicoRepository.save(digitoUnico);

        assertEquals(digitoUnico.getEntradaCalculo(), "9875");
        assertEquals(digitoUnico.getResultado(), Integer.valueOf(2));
        assertNotNull(digitoUnico.getId());
    }

}

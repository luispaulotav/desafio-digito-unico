package br.com.desafio.digito.unico.resource;

import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.CriptografiaDTO;
import br.com.desafio.digito.unico.service.implementation.CriptografiaServiceImp;
import br.com.desafio.digito.unico.utils.MockUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(CriptografiaResource.class)
public class CriptografiaResourceTeste {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CriptografiaServiceImp criptografiaServiceImp;

    ObjectMapper objectMapper;

    private CriptografiaDTO criptografiaDTO;

    private Usuario usuario;

    @Before
    public void init(){
        objectMapper = new ObjectMapper();
        usuario = MockUtils.montaUsuario();
        usuario.setId(1);
        criptografiaDTO = MockUtils.montaCriptografiaDTO();
    }

    @Test
    public void gerarChavePulicaTeste() throws Exception {
        Mockito.when(this.criptografiaServiceImp.gerarChavePublica(usuario.getId())).thenReturn(criptografiaDTO);
        this.mvc.perform(MockMvcRequestBuilders.get("/criptografias/gerar/chavepublica/"+usuario.getId())
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$['chavePublica']", Matchers.equalToIgnoringWhiteSpace(criptografiaDTO.getChavePublica())));
    }

    @Test
    public void descriptografarTeste() throws Exception {
        Mockito.when(this.criptografiaServiceImp.validaChavePublicaParaDescriptografar(criptografiaDTO)).thenReturn(criptografiaDTO);
        this.mvc.perform(MockMvcRequestBuilders.post("/criptografias/descriptografar")
                .content(objectMapper.writeValueAsString(criptografiaDTO))
                .characterEncoding("UTF-8")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$['chavePublica']", Matchers.equalToIgnoringWhiteSpace(criptografiaDTO.getChavePublica())))
                .andExpect(MockMvcResultMatchers.jsonPath("$['dadosDescriptografados']", Matchers.equalToIgnoringWhiteSpace(criptografiaDTO.getDadosDescriptografados())));
    }

}

package br.com.desafio.digito.unico.service;

import br.com.desafio.digito.unico.exception.NotFoundException;
import br.com.desafio.digito.unico.model.Criptografia;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.CriptografiaDTO;
import br.com.desafio.digito.unico.repository.CriptografiaRepository;
import br.com.desafio.digito.unico.repository.UsuarioRepository;
import br.com.desafio.digito.unico.service.implementation.CriptografiaServiceImp;
import br.com.desafio.digito.unico.utils.MockUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CriptografiaServiceImpTest {

    @InjectMocks
    private CriptografiaServiceImp criptografiaServiceImp;

    @Mock
    private CriptografiaRepository criptografiaRepository;

    @Mock
    private UsuarioRepository usuarioRepository;

    private Criptografia criptografia;

    private CriptografiaDTO criptografiaDTO;

    private Usuario usuario;

    @Before
    public void init() {
        usuario = MockUtils.montaUsuario();
        usuario.setId(1);
        criptografia = MockUtils.montaCriptografia();
        criptografiaDTO = MockUtils.montaCriptografiaDTO();
    }

    @Test
    public void gerarChavePublicaTeste() throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        when(usuarioRepository.findById(usuario.getId())).thenReturn(Optional.of(usuario));
        when( criptografiaRepository.save(criptografia)).thenReturn(criptografia);

        criptografiaServiceImp.gerarChavePublica(usuario.getId());
        Assert.assertNotNull(criptografia.getChavePublica());
        Assert.assertNotNull(criptografia.getChavePrivada());
    }

    @Test(expected = NotFoundException.class)
    public void gerarChavePublicaComIdUsuarioNulo() throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
      criptografiaServiceImp.gerarChavePublica(new Usuario().getId());
    }

    @Test(expected = BadPaddingException.class)
    public void validaChavePublicaParaDescriptografarComDaodsIvalido() throws BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException {
        when(criptografiaRepository.findCriptografiaByChavePublica(criptografiaDTO.getChavePublica())).thenReturn(criptografia);

        criptografiaServiceImp.validaChavePublicaParaDescriptografar(criptografiaDTO);
        Assert.assertNotNull(criptografiaDTO.getChavePublica());
    }

}

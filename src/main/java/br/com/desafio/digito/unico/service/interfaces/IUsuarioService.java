package br.com.desafio.digito.unico.service.interfaces;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IUsuarioService {

    Page<Usuario> buscarTodos(Pageable pageable);

    Usuario salvar(Usuario usuario);

    Usuario editar(Integer idUsuario, Usuario usuario);

    Boolean excluir(Integer idUsuario);

    Optional<Usuario> buscarUsuario(Integer idUsuario);

    List<DigitoUnico> buscarDigitoUnicoPorIdUsuario(Integer idUsuario);
}

package br.com.desafio.digito.unico.repository;

import br.com.desafio.digito.unico.model.Criptografia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CriptografiaRepository extends JpaRepository<Criptografia, Integer> {

    Criptografia findCriptografiaByChavePublica(@Param("chavePublica") String chavePublica);
}

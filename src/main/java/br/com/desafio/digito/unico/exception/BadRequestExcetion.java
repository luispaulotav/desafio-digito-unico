package br.com.desafio.digito.unico.exception;

public class BadRequestExcetion extends RuntimeException {

    public BadRequestExcetion(String message) {
        super(message);
    }
}

package br.com.desafio.digito.unico.service.interfaces;

import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.CriptografiaDTO;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;

public interface ICriptografiaService {

    CriptografiaDTO gerarChavePublica(Integer idUsuario) throws NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            NoSuchPaddingException, IllegalBlockSizeException;

    CriptografiaDTO validaChavePublicaParaDescriptografar(CriptografiaDTO criptografiaDTO) throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException, IOException;
}

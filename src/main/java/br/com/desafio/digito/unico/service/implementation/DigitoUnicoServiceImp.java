package br.com.desafio.digito.unico.service.implementation;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.DigitoUnicoDTO;
import br.com.desafio.digito.unico.repository.DigitoUnicoRepository;
import br.com.desafio.digito.unico.repository.UsuarioRepository;
import br.com.desafio.digito.unico.service.interfaces.IDigitoUnicoService;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.Cacheable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

@Service
public class DigitoUnicoServiceImp implements IDigitoUnicoService {

    private final DigitoUnicoRepository digitoUnicoRepository;
    private final UsuarioRepository usuarioRepository;

    public DigitoUnicoServiceImp(DigitoUnicoRepository digitoUnicoRepository, UsuarioRepository usuarioRepository) {
        this.digitoUnicoRepository = digitoUnicoRepository;
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public DigitoUnico validarCalculoDigitoUnico(DigitoUnicoDTO digitoUnicoDTO) {
        DigitoUnico digitoUnico = montaDigitoUnico(digitoUnicoDTO);
        Integer resultado = null;

        if(digitoUnicoDTO.getNumeroVezes() != null) {
           resultado = calculaDigitoUnico(digitoUnicoDTO.getEntradaCalculo(), digitoUnicoDTO.getNumeroVezes());
        } else {
            resultado = calculaDigitoUnico(digitoUnicoDTO.getEntradaCalculo());
        }

        digitoUnico.setResultado(resultado);
        digitoUnico = digitoUnicoRepository.save(digitoUnico);
        vinculaDigitoUnicoComUsuario(digitoUnicoDTO.getIdUsuario(), digitoUnico);
        return digitoUnico;
    }

    private void vinculaDigitoUnicoComUsuario(Integer idUsuario, DigitoUnico digitoUnico) {
        Optional<Usuario> usuario = null;
        if(idUsuario != null && digitoUnico != null) {
            usuario = usuarioRepository.findById(idUsuario);
            if(usuario.isPresent()) {
                usuario.get().getDigitoUnicos().add(digitoUnico);;
                usuarioRepository.save(usuario.get());
            }
        }
    }

    private DigitoUnico montaDigitoUnico(DigitoUnicoDTO digitoUnicoDTO) {
        DigitoUnico digitoUnico = new DigitoUnico();
        if (digitoUnicoDTO.getNumeroVezes() != null) {
            String entradaCalculo = concatenaDigitoUnico(digitoUnicoDTO.getEntradaCalculo(), digitoUnicoDTO.getNumeroVezes());
            digitoUnico.setEntradaCalculo(String.valueOf(entradaCalculo));
        } else {
            digitoUnico.setEntradaCalculo(String.valueOf(digitoUnicoDTO.getEntradaCalculo()));
        }
        return digitoUnico;
    }

    public Integer calculaDigitoUnico(String entradaCalculo) {
        String resultado = null;
        if(entradaCalculo.length() == 1) {
            return Integer.parseInt(entradaCalculo);
        }
        resultado = somaDigitoUnico(entradaCalculo);
        return calculaDigitoUnico(resultado);
    }

    public Integer calculaDigitoUnico(String entradaCalculo, Integer numeroVezes) {
        String resultado = null;
        entradaCalculo = concatenaDigitoUnico(entradaCalculo, numeroVezes);
        if(entradaCalculo.length() == 1) {
            return Integer.parseInt(entradaCalculo);
        }
        resultado = somaDigitoUnico(entradaCalculo);
        return calculaDigitoUnico(resultado);
    }

    private String concatenaDigitoUnico(String entradaCalculo, Integer numeroVezes) {
        String digitoUnicoConcatenados = entradaCalculo;
        for (int j = 1; j < numeroVezes; j++) {
            digitoUnicoConcatenados += entradaCalculo;
        }
        return digitoUnicoConcatenados;
    }

    private String somaDigitoUnico(String tamanho ) {
        int resultado =0;
        for (int i = 0; i <= tamanho.length()-1; i++) {
            resultado += Integer.parseInt(String.valueOf(tamanho.charAt(i)));
        }
        return String.valueOf(resultado);
    }

    @Cacheable("digitoUnicos")
    public List<DigitoUnico> buscarTodosDigitoUnicos() {
         return digitoUnicoRepository.findAll()
                 .stream()
                 .sorted(comparing(DigitoUnico::getId).reversed()).limit(10).collect(Collectors.toList());
    }
}

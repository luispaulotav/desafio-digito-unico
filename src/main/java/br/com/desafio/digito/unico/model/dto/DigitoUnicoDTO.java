package br.com.desafio.digito.unico.model.dto;

import lombok.*;

import javax.validation.constraints.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DigitoUnicoDTO {

    @Min(value = 1, message = "O campo entradaCalculo deve ser no minimo 1.")
    @Max(value = 1000000, message = "O campo entradaCalculo deve ser no maximo 1000000.")
    @NotBlank(message = "entradaCalculo não deve estar em branco.")
    @NotNull(message = "entradaCalculo não deve ser nulo.")
    @Pattern(regexp = "[0-9]+", message = "Deve conter apenas números no campo entradaCalculo.")
    private String entradaCalculo;

    @Min(value = 1, message = "O campo entradaCalculo deve ser no minimo 1.")
    @Max(value = 1000000, message = "O campo entradaCalculo deve ser no maximo 1000000.")
    private Integer numeroVezes;

    private Integer idUsuario;
}

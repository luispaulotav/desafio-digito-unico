package br.com.desafio.digito.unico.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Lob;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CriptografiaDTO {

    @Lob
    @NotBlank(message = "chavePublica não deve estar em branco.")
    @NotNull(message = "chavePublica não deve ser nulo.")
    String chavePublica;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    String dadosDescriptografados;
}

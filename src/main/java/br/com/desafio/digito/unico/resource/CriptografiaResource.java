package br.com.desafio.digito.unico.resource;

import br.com.desafio.digito.unico.model.dto.CriptografiaDTO;
import br.com.desafio.digito.unico.service.implementation.CriptografiaServiceImp;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;


@RestController
@RequestMapping("/criptografias")
public class CriptografiaResource {

    @Autowired
    private CriptografiaServiceImp criptografiaServiceImp;

    @GetMapping(value = "/gerar/chavepublica/{idUsuario}", produces = "application/json")
    @ApiOperation(value = "Gera chave publica a partir do id do usuário.", response = CriptografiaDTO.class)
    public ResponseEntity<?> gerarChavePulica(@ApiParam(value="idUsuario") @PathVariable("idUsuario") Integer idUsuario) {
        try {
            return ResponseEntity.ok(this.criptografiaServiceImp.gerarChavePublica(idUsuario));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping(value = "/descriptografar", produces = "application/json")
    @ApiOperation(value = "Descriptografar dados do usuário a partir da chave pública.", response = CriptografiaDTO.class)
    public ResponseEntity<?> descriptografar(@RequestBody CriptografiaDTO criptografiaDTO) {
        try {
            return ResponseEntity.ok(this.criptografiaServiceImp.validaChavePublicaParaDescriptografar(criptografiaDTO));
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }
}

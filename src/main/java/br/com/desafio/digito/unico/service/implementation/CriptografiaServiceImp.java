package br.com.desafio.digito.unico.service.implementation;

import br.com.desafio.digito.unico.exception.NotFoundException;
import br.com.desafio.digito.unico.model.Criptografia;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.model.dto.CriptografiaDTO;
import br.com.desafio.digito.unico.repository.CriptografiaRepository;
import br.com.desafio.digito.unico.repository.UsuarioRepository;
import br.com.desafio.digito.unico.service.interfaces.ICriptografiaService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Optional;

@Service
public class CriptografiaServiceImp  implements ICriptografiaService {

    private final CriptografiaRepository criptografiaRepository;

    private final UsuarioRepository usuarioRepository;

    public CriptografiaServiceImp(CriptografiaRepository criptografiaRepository, UsuarioRepository usuarioRepository) {
        this.criptografiaRepository = criptografiaRepository;
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public CriptografiaDTO gerarChavePublica(Integer idUsuario) throws NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            NoSuchPaddingException, IllegalBlockSizeException {
        Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);

        if(!usuario.isPresent()){
            throw new NotFoundException("Usuário não encontrado.");
        }
        String chavePublica = gerarCriptografia(usuario.get());

        return getCriptografiaDTO(chavePublica);
    }

    @Override
    public CriptografiaDTO validaChavePublicaParaDescriptografar(CriptografiaDTO criptografiaDTO) throws InvalidKeySpecException, NoSuchAlgorithmException,
            IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchPaddingException {
        Criptografia criptografia = criptografiaRepository.findCriptografiaByChavePublica(criptografiaDTO.getChavePublica());

        if(criptografia == null) {
            throw new NotFoundException("Esta chave pública não é valido");
        }

        PrivateKey chavePrivada = converteStringParaChavePrivada(criptografia.getChavePrivada());
        String dadosDescriptografados = descriptografar(criptografia.getDadosCriptografados(),chavePrivada);
        criptografiaDTO.setDadosDescriptografados(dadosDescriptografados);
        return criptografiaDTO;
    }

    private String descriptografar(byte[] DadosCriptografados, PrivateKey chavePrivada) throws NoSuchPaddingException,
            NoSuchAlgorithmException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException {
        byte[] textoDescriptografado = null;
        final Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
        textoDescriptografado = cipher.doFinal(DadosCriptografados);

        return new String(textoDescriptografado);
    }

    private String gerarCriptografia(Usuario usuario) throws NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException {
        KeyPair chavePar = gerarChavePar();
        String dadosUsuario = montaTextoDadosUsuario(usuario);
        byte[] dadosCriptografados = criptografar(dadosUsuario, chavePar.getPublic());
        salvarCriptografia(chavePar, dadosCriptografados);
        String chavePublica = converteChaveParaString(chavePar.getPublic().getEncoded());
        return chavePublica;
    }

    private PrivateKey converteStringParaChavePrivada(String chavePrivada) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] chavePrivadaBytes = Base64.decodeBase64(chavePrivada);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(chavePrivadaBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = kf.generatePrivate(spec);
        return privateKey;
    }

    private KeyPair gerarChavePar() throws NoSuchAlgorithmException {
        KeyPairGenerator gerador = KeyPairGenerator.getInstance("RSA");
        gerador.initialize(2048);
        KeyPair chavePar = gerador.generateKeyPair();
        return chavePar;
    }

    private byte[] criptografar(String dados, PublicKey chavePublica) throws InvalidKeyException,
            NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException {
        byte[] textoCriptografado = null;
        Cipher cifra = Cipher.getInstance("RSA");
        cifra.init(Cipher.ENCRYPT_MODE, chavePublica);
        textoCriptografado = cifra.doFinal(dados.getBytes());
        return textoCriptografado;
    }

    private String converteChaveParaString(byte[] encoded) {
        return Base64.encodeBase64String(encoded);
    }

    private void salvarCriptografia(KeyPair chavePar, byte[] dadosCriptografados) {
        Criptografia criptografia = new Criptografia();
        criptografia.setDadosCriptografados(dadosCriptografados);
        String chavePrivada = converteChaveParaString(chavePar.getPrivate().getEncoded());
        criptografia.setChavePrivada(chavePrivada);
        String chavePublica = converteChaveParaString(chavePar.getPublic().getEncoded());
        criptografia.setChavePublica(chavePublica);
        criptografiaRepository.save(criptografia);
    }

    private String montaTextoDadosUsuario(Usuario usuario) {
        return "Nome:"+ usuario.getNome()+" - "+"Email:"+usuario.getEmail();
    }

    private CriptografiaDTO getCriptografiaDTO(String chavePublica) {
        CriptografiaDTO criptografiaDTO = new CriptografiaDTO();
        criptografiaDTO.setChavePublica(chavePublica);
        return criptografiaDTO;
    }
}

package br.com.desafio.digito.unico.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class DigitoUnico extends BaseEntity {

    @NotBlank(message = "entradaCalculo não deve estar em branco.")
    @Pattern(regexp = "[0-9]+", message = "Deve conter apenas números no campo entradaCalculo.")
    private String entradaCalculo;

    private Integer resultado;

}

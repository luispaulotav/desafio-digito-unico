package br.com.desafio.digito.unico.repository;

import br.com.desafio.digito.unico.model.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Integer> {
}

package br.com.desafio.digito.unico.resource;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.service.implementation.UsuarioServiceImp;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

    @Autowired
    private UsuarioServiceImp usuarioService;

    @PostMapping(value = "/salvar", produces = "application/json")
    @ApiOperation(value = "Salvar Usuário.", response = Usuario.class)
    public ResponseEntity<?> salvarUsuario(@Valid @RequestBody Usuario usuario) {
        return ResponseEntity.ok(this.usuarioService.salvar(usuario));
    }

    @PutMapping(value = "/editar/{id}", produces = "application/json")
    @ApiOperation(value = "Atualizar Usuário.", response = Usuario.class)
    public ResponseEntity<?> editarUsuario(@ApiParam(value="id") @PathVariable("id") Integer idUsuario,
                                           @Valid @RequestBody Usuario usuario) {
        return ResponseEntity.ok(this.usuarioService.editar(idUsuario, usuario));
    }

    @DeleteMapping(value = "/excluir/{id}", produces = "application/json")
    @ApiOperation(value = "Excluir Usuário por Id.", response = Usuario.class)
    public ResponseEntity<?> excluirUsuario(@ApiParam(value="id") @PathVariable("id") Integer idUsuario) {
        return ResponseEntity.ok(this.usuarioService.excluir(idUsuario));
    }

    @GetMapping(value = "/buscar/{id}", produces = "application/json")
    @ApiOperation(value = "Buscar Usuário por Id.", response = Usuario.class)
    public ResponseEntity<?> buscarUsuario(@ApiParam(value="id") @PathVariable("id") Integer idUsuario) {
        return ResponseEntity.ok(this.usuarioService.buscarUsuario(idUsuario));
    }

    @GetMapping(value = "/buscartodos", produces = "application/json")
    @ApiOperation(value = "Buscar Todos Usuários", response = Usuario.class)
    public ResponseEntity<?> buscaTodosUsuarios(Pageable pageable) {
        return ResponseEntity.ok(this.usuarioService.buscarTodos(pageable));
    }

    @GetMapping(value = "/buscarresultado/{idUsuario}", produces = "application/json")
    @ApiOperation(value = "Buscar todos resultados do digito unico por usuário.", response = DigitoUnico.class)
    public ResponseEntity<?> buscarResultadoPorUsuario(@ApiParam(value="idUsuario") @PathVariable("idUsuario") Integer idUsuario) {
        return ResponseEntity.ok(this.usuarioService.buscarDigitoUnicoPorIdUsuario(idUsuario));
    }
}

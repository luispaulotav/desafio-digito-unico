package br.com.desafio.digito.unico.service.interfaces;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.dto.DigitoUnicoDTO;

import java.util.List;

public interface IDigitoUnicoService {
    DigitoUnico validarCalculoDigitoUnico(DigitoUnicoDTO digitoUnicoDTO);
}

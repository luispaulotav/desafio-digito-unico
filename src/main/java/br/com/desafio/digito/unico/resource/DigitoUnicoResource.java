package br.com.desafio.digito.unico.resource;

import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.dto.DigitoUnicoDTO;
import br.com.desafio.digito.unico.service.implementation.DigitoUnicoServiceImp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/digitounicos")
public class DigitoUnicoResource {

    @Autowired
    private DigitoUnicoServiceImp digitoUnicoServiceImp;

    @PostMapping(value = "/calcular", produces = "application/json")
    @ApiOperation(value = "Calcula a entrada de calculo e retorna um digito unico.", response = DigitoUnico.class)
    public ResponseEntity<?> calcularDigitoUnico(@Valid @RequestBody DigitoUnicoDTO digitoUnicoDTO) {
        return ResponseEntity.ok(this.digitoUnicoServiceImp.validarCalculoDigitoUnico(digitoUnicoDTO));
    }

    @GetMapping(value = "/cache/buscartodos", produces = "application/json")
    @ApiOperation(value = "Busca os 10 ultimos calculos salvos em cache.", response = DigitoUnico.class)
    public ResponseEntity<?> buscaTodosCalculosEmCache() {
        return ResponseEntity.ok(this.digitoUnicoServiceImp.buscarTodosDigitoUnicos());
    }
}

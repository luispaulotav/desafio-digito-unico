package br.com.desafio.digito.unico.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Criptografia extends BaseEntity {

    @Column(length = 1000)
    @Lob
    private String chavePrivada;

    @Column(length = 1000)
    @Lob
    private String chavePublica;

    @Lob
    private byte[] dadosCriptografados;

}


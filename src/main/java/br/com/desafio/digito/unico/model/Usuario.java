package br.com.desafio.digito.unico.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Usuario extends BaseEntity {

    @NotBlank(message = "Nome não deve estar em branco.")
    @NotNull(message = "Nome não deve ser nulo.")
    private String nome;

    @Email(message = "Endereço de e-mail enviado em um formato inválido.")
    @NotBlank(message = "E-mail não deve estar em branco.")
    @NotNull(message = "E-mail não deve ser nulo.")
    private String email;

    @OneToMany
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<DigitoUnico> digitoUnicos;

}

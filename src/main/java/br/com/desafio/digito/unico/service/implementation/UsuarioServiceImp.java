package br.com.desafio.digito.unico.service.implementation;

import br.com.desafio.digito.unico.exception.BadRequestExcetion;
import br.com.desafio.digito.unico.exception.NotFoundException;
import br.com.desafio.digito.unico.model.DigitoUnico;
import br.com.desafio.digito.unico.model.Usuario;
import br.com.desafio.digito.unico.repository.UsuarioRepository;
import br.com.desafio.digito.unico.service.interfaces.IUsuarioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UsuarioServiceImp implements IUsuarioService {

    private final UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioServiceImp(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public Page<Usuario> buscarTodos(Pageable pageable) {
        return this.usuarioRepository.findAll(pageable);
    }

    @Override
    public Usuario salvar(Usuario usuario) {
        return this.usuarioRepository.save(usuario);
    }

    @Override
    public Usuario editar(Integer idUsuario, Usuario usuario) {
        if (idUsuario == null) {
            log.error("Erro ao editar usuário, idUsuario é nulo.");
            throw new BadRequestExcetion("Erro ao editar usuário, idUsuario é nulo.");
        }
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);

        if(!optionalUsuario.isPresent()) {
            log.error("usuário não encontrado.");
            throw new NotFoundException("usuário não encontrado.");
        }
        optionalUsuario.get().setEmail(usuario.getEmail());
        optionalUsuario.get().setNome(usuario.getNome());
        return this.usuarioRepository.save(optionalUsuario.get());
    }

    @Override
    public Boolean excluir(Integer idUsuario) {
        if (idUsuario == null) {
            log.error("Erro ao editar usuário, idUsuario é nulo.");
            throw new BadRequestExcetion("Erro ao editar usuário, idUsuario é nulo.");
        }
        Optional<Usuario> optionalUsuario = buscarUsuario(idUsuario);

        if(!optionalUsuario.isPresent()) {
            log.error("usuário não encontrado.");
            throw new NotFoundException("usuário não encontrado.");
        }
        this.usuarioRepository.deleteById(idUsuario);
        return true;
    }

    @Override
    public Optional<Usuario> buscarUsuario(Integer idUsuario) {
        if (idUsuario == null) {
            log.error("Erro ao buscar usuário, idUsuario é nulo.");
            throw new BadRequestExcetion("Erro ao buscar usuário, idUsuario é nulo.");
        }
        return this.usuarioRepository.findById(idUsuario);
    }

    @Override
    public List<DigitoUnico> buscarDigitoUnicoPorIdUsuario(Integer idUsuario) {
        Optional<Usuario> usuario = null;
        if (idUsuario != null) {
            usuario = usuarioRepository.findById(idUsuario);
        }
        if (!usuario.isPresent()) {
            throw new NotFoundException("Não foi encontrado nenhum usuario por este ID.");
        }
        if (usuario.get().getDigitoUnicos().isEmpty()) {
            throw new NotFoundException("Não foi encontrado nenhum calculo de digito unico para este usuário.");
        }
        return usuario.get().getDigitoUnicos();
    }
}

# Desafio Digito Unico API

A aplicação **Desafio Digito Unico API** é uma aplicação para gerenciamento de usuário, cálculos de digito únicos, cache e criptografia.

# Tecnologias

Para criar o sistema foram utilizados as seguintes ferramentas/frameworks:

- Backend com Spring-boot v2.3+ e Java v8

# Composição da Stack

A Stack do Desafio Digito Unico API é composta por 1 aplicação (apenas backend), que contém a conexão com o banco de dados, 
e gerenciamento das regras de negócio do sistema.

## O que preciso para subir a aplicação

- Gerenciador de dependencias e build, Maven.

- Java 8.

## Gerando o Pacote
Sendo um projeto Maven, execute os goals clean e install na raiz do projeto para baixar as dependências e gerar jar do projeto

     #!/desafio-digito-unico
     $ mvn clean install
     
## Executando o Jar

Como se trata de um projeto Spring Boot, podemos simplismente executar o jar que foi gerado na pasta target e a 
aplicação irá subir em um tomcat embedded.

    #!/desafio-digito-unico/target
    $ java -jar desafio-digito-unico-0.0.1-SNAPSHOT.jar
    
## Executando os Testes Unitários

    #!/desafio-digito-unico
    $ mvn test    

Configuração da porta da api se encontra no application.yml:
		
	server:
	    port: 9000

Pronto, a aplicação deve estar online na porta 9000.

## Consumir API

Para consumir a API basta acessar por exemplo a url para buscar todos os usuários http://localhost:9000/digitounico/v1/usuarios/buscartodos.

ou:

Consumir através da url para buscar todos os usuários http://desafio-banco-inter.herokuapp.com/digitounico/v1/usuarios/buscartodos,
que está hospedado na plataforma em nuvem Heroku.

### Exemplos:

#### Create Usuarios/salvar:

Entrada:

http://localhost:9000/digitounico/v1/usuarios/salvar

    {
      "nome": "Joao da Silva",
      "email": "joao@teste.com"
    }
    
Saída: 200 OK

    {
      "id": 1,
      "nome": "Joao da Silva",
      "email": "joao@teste.com"
    }
    

#### Calcular/DigitoUnico:

Entrada:

http://desafio-banco-inter.herokuapp.com/digitounico/v1/digitounicos/calcular

       {
        "entradaCalculo": "9875",
        "numeroVezes": "4"   	
       }
        
Saída: OK

       {
           "id": 1,
           "entradaCalculo": "9875987598759875",
           "resultado": 8
       }